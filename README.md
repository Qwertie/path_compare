# PathCompare

A gem that takes 2 linestring paths and returns a value comparing how similar they are.

This library is not super accurate but it should be good enough for a lot of things. Please don't use this for anything needing high accuracy results.

## Installation

TODO

## Usage

TODO

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
