require "test_helper"

class PathCompareTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::PathCompare::VERSION
  end

  def test_it_passes_correct_paths
    Dir.entries("./test/test_paths").reject{|i| i == '.' || i == '..'}.each do |test_path|
      original = File.readlines("./test/test_paths/#{test_path}/original.geojson").join("")
      Dir.entries("./test/test_paths/#{test_path}/pass").reject{|i| i == '.' || i == '..'}.each do |passing_path|
        file_path = "./test/test_paths/#{test_path}/pass/#{passing_path}"
        new_path = File.readlines(file_path).join("")

        p "comparing", PathCompare.compare(original,new_path)

        unless PathCompare.compare(original,new_path) >= 0.0002
          puts "Failed '#{file_path}' when it should have passed"
        end
        assert PathCompare.compare(original,new_path) >= 0.0002
      end
    end
  end

  def test_it_fails_wrong_paths
    Dir.entries("./test/test_paths").reject{|i| i == '.' || i == '..'}.each do |test_path|
      original = File.readlines("./test/test_paths/#{test_path}/original.geojson").join("")
      Dir.entries("./test/test_paths/#{test_path}/fail").reject{|i| i == '.' || i == '..'}.each do |passing_path|
        file_path = "./test/test_paths/#{test_path}/fail/#{passing_path}"
        new_path = File.readlines(file_path).join("")

        p "comparing", PathCompare.compare(original,new_path)

        unless PathCompare.compare(original,new_path) <= 0.0002
          puts "Passed '#{file_path}' when it should have failed"
        end
        assert PathCompare.compare(original,new_path) <= 0.0002
      end
    end
  end
end
